#ifndef   EXAMPLE_ENGINE_HPP_INCLUDED
#define   EXAMPLE_ENGINE_HPP_INCLUDED


#include "Header/Engine/Interfaces/IEngine.hpp"
#include "Header/Engine/Screens/SplashScreen.hpp"

namespace cn {
	class ExampleEngine :
		public cn::IEngine
	{
	public:
		ExampleEngine(const std::string title = "Example");
		virtual ~ExampleEngine();
	protected:
		/**
		* InitAssetHandlers is responsible for registering custom IAssetHandler
		* derived classes for a specific game application.
		*/
		virtual void InitAssetHandlers(void);

		/**
		* InitScreenFactory is responsible for initializing any IScreen derived
		* classes with the ScreenManager class that will be used to create new
		* IScreen derived classes as requested.
		*/
		virtual void InitScreenFactory(void);

		/**
		* HandleCleanup is responsible for performing any custom last minute
		* Application cleanup steps before exiting the Application.
		*/
		virtual void HandleCleanup(void);
	};
}
#endif //EXAMPLE_ENGINE_HPP_INCLUDED