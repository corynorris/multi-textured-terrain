#ifndef   CN_TERRAIN_HPP_INCLUDED
#define   CN_TERRAIN_HPP_INCLUDED

#include <GL/glew.h>
#include <SFML/OpenGL.hpp>
#include "Header/Engine/Assets/ImageAsset.hpp"
#include "Header/Engine/Assets/ShaderAsset.hpp"
#include "Header/Engine/Assets/TextureAsset.hpp"
#include "VertexBufferObject.hpp"
#include "Lighting.hpp"

namespace cn {
	class Terrain
	{
	public:
		Terrain();
		~Terrain();
		void SetScale(glm::mat4 scale_matrix);
		void LoadHeightmapFromFile(std::string file_name);
		void Draw(tdogl::Camera &camera, Lighting& lighting_);

	private:
		float ScaleToOpenGLCoordinates(float i, float max);
		float GrayScale(sf::Color pixel);
		void AddElementCoords(unsigned int x, unsigned int z);
		void StitchTriangleStrip(unsigned int z);
		void CalculateNormals();

		// could just increment an int, thought this was more clear
		unsigned int Transform2Dto1D(unsigned int x, unsigned int z);
		unsigned int Transform2Dto1D(unsigned int x, unsigned int z, unsigned int rows);

		// Shader Program
		tdogl::ShaderProgram *terrain_shader_;
		glm::mat4 scale_matrix_;

		// Vertex buffers and arrays
		GLuint vao_;
		std::vector<glm::vec3> vertex_data_;
		std::vector<glm::vec2> texture_data_;
		std::vector<glm::vec3> normal_data_;

		VertexBufferObject element_data_;
		VertexBufferObject vertex_texture_normal_data_;

		// Image and image properties
		sf::Image terrain_heightmap_;
		unsigned int rows_;
		unsigned int cols_;

		// Assets
		ShaderAsset vertex_shader_;
		ShaderAsset fragment_shader_;
		TextureAsset water_texture_;
		TextureAsset sand_texture_;
		TextureAsset sandgrass_texture_;
		TextureAsset rock_texture_;
	};
}
#endif // CN_TERRAIN_HPP_INCLUDED