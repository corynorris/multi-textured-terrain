#ifndef   CN_LIGHTING_HPP_INCLUDED
#define   CN_LIGHTING_HPP_INCLUDED
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <vector>

namespace cn
{

	struct LightSource {
		glm::vec3 position;
		glm::vec3 intensities; //a.k.a. the color of the light
	};

	class Lighting
	{
	public:
		Lighting();
		virtual ~Lighting();
		void AddLightSource(LightSource light_source);
		LightSource GetLightSource();

	private:
		//std::vector<LightSource> lights_;
		LightSource lights_;
	};

}
#endif //CN_LIGHTING_HPP_INCLUDED