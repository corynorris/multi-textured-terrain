/*
main

Copyright 2012 Thomas Dalling - http://tomdalling.com/

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

// standard C++ libraries
#include "Header/Core.hpp"
#include "ExampleEngine.hpp"
#include <cassert>
#include <cstddef>
#include <iostream>


int main(int argc, char* argv[])
{
	// Inherit Engine and change the asset, render, etc functions as desired
	// TODO: implement stat manager
	// TODO: Look into EventManager
	// TODO: Find a better way to handle instances of assets

	int exit_code = cn::StatusNoError;

	cn::FileLogger logger("log_output.txt",true);

	cn::IEngine* engine = new (std::nothrow) cn::ExampleEngine;
	assert(NULL != engine && "main() Can't create Application");
	engine->ProcessArguments(argc, argv); //not yet implemented

	exit_code = engine->Run();

	delete engine;
	engine = NULL;
	return exit_code;
}