#ifndef   CN_VBO_HPP_INCLUDED
#define   CN_VBO_HPP_INCLUDED

#include "GL/glew.h"
#include <SFML/OpenGL.hpp>
#include <vector>

namespace cn {

	class VertexBufferObject
	{
	public:
		VertexBufferObject();
		VertexBufferObject(int size);
		virtual ~VertexBufferObject();

		GLuint GetLocation();
		void AddData(void* ptrData, UINT uiDataSize);
		void BindBuffer(GLenum buffer_target = GL_ARRAY_BUFFER);		
		void UploadDataToGPU(GLenum usage_pattern = GL_STATIC_DRAW);

		// Implement these if I never need to change a buffer
		// void* MapBuffer(GLenum usage_pattern = GL_STATIC_DRAW);
		// void* MapBuffer(GLenum usage_pattern = GL_STATIC_DRAW, UINT uiOffset, UINT uiLength);
		// void UnmapBuffer();


	private:
		GLuint vbo_;
		GLenum buffer_target_;
		GLenum usage_pattern_;
		std::vector<BYTE> data_;
	};
}
#endif //CN_VBO_HPP_INCLUDED