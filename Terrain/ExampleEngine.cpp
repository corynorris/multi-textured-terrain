#include "ExampleEngine.hpp"
#include "Header/Engine/Screens/SplashScreen.hpp"
#include "WorldScreen.hpp"

namespace cn {
	ExampleEngine::ExampleEngine(const std::string title) :
	cn::IEngine(title)
	{
	}


	ExampleEngine::~ExampleEngine()
	{
	}


	void ExampleEngine::InitAssetHandlers(void)
	{
		// No custom asset handlers needed or provided
	}

	void ExampleEngine::InitScreenFactory(void)
	{
		// Add Menu State as the next active state
		screen_manager_.AddActiveScreen(new cn::WorldScreen(*this, "World"));
		//screen_manager_.AddActiveScreen(new cn::SplashScreen(*this, "Splash", 3.5f));
	}

	void ExampleEngine::HandleCleanup(void)
	{
		// No custom cleanup needed
	}
}