#include "VertexBufferObject.hpp"

namespace cn {
	// Generate a vbo
	VertexBufferObject::VertexBufferObject()
	{
		glGenBuffers(1, &vbo_);
	}

	// Let a buffer reserve size if it knows how large it will be
	VertexBufferObject::VertexBufferObject(int size)
	{
		glGenBuffers(1, &vbo_);
		data_.reserve(size);
	}

	VertexBufferObject::~VertexBufferObject()
	{
		glDeleteBuffers(1, &vbo_);
		data_.clear();
	}

	void VertexBufferObject::AddData(void* data, UINT data_size)
	{
		data_.insert(data_.end(), (BYTE*)data, (BYTE*)data + data_size);
	}
	/*-----------------------------------------------

	Name:	BindVBO

	Params:	a_iBufferType - buffer type (GL_ARRAY_BUFFER, ...)

	Result:	Binds this VBO.

	/*---------------------------------------------*/
	void VertexBufferObject::BindBuffer(GLenum buffer_target)
	{
		buffer_target_ = buffer_target;
		glBindBuffer(buffer_target, vbo_);
	}

	/*-----------------------------------------------

	Name:	UploadDataToGPU

	Params:	iUsageHint - GL_STATIC_DRAW, GL_DYNAMIC_DRAW...

	Result:	Sends data to GPU.

	/*---------------------------------------------*/
	void VertexBufferObject::UploadDataToGPU(GLenum usage_pattern)
	{
		usage_pattern_ = usage_pattern;
		// Because we're using BYTE, data_.size() == the size of the data
		// eg, float would require data_.size()*sizeof(float), but here *sizeof(data_[0]) would be redundant
		glBufferData(buffer_target_, data_.size(), &data_[0], usage_pattern);
		data_.clear();
	}

	GLuint VertexBufferObject::GetLocation()
	{
		return vbo_;
	}
}