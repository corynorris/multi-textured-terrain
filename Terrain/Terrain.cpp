#include "Terrain.hpp"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

//TODO: Bind textures - done
//TODO: change vertex shader to use camera - done
//TODO: fix shaders, because I've undoubtedly broken them - done
//TODO: area lighting
//TODO: point lighting
//TODO: skybox?
//TODO: implement QuadTree
//TODO: GPU-Based Geometry Clipmaps

namespace cn {
	Terrain::Terrain() :
		scale_matrix_(glm::scale(glm::mat4(1.0), glm::vec3(200.0f, 100.0f, 200.0f))),
		vertex_shader_("C:/Users/Cory/Google Drive/Code/Resources/Shaders/Terrain.vert", GL_VERTEX_SHADER,
		AssetLoadNow, AssetLoadFromFile, AssetDropAtZero),
		fragment_shader_("C:/Users/Cory/Google Drive/Code/Resources/Shaders/Terrain.frag", GL_FRAGMENT_SHADER,
		AssetLoadNow, AssetLoadFromFile, AssetDropAtZero),
		sand_texture_("C:/Users/Cory/Google Drive/Code/Resources/Texture/sand.jpg",
		AssetLoadNow, AssetLoadFromFile, AssetDropAtZero),
		sandgrass_texture_("C:/Users/Cory/Google Drive/Code/Resources/Texture/sand_grass_02.jpg",
		AssetLoadNow, AssetLoadFromFile, AssetDropAtZero),
		rock_texture_("C:/Users/Cory/Google Drive/Code/Resources/Texture/rock_2_4w.jpg",
		AssetLoadNow, AssetLoadFromFile, AssetDropAtZero),
		water_texture_("C:/Users/Cory/Google Drive/Code/Resources/Texture/water.jpg",
		AssetLoadNow, AssetLoadFromFile, AssetDropAtZero)
	{
	}

	Terrain::~Terrain()
	{
		delete terrain_shader_;
		terrain_shader_ = NULL;
	}

	void Terrain::SetScale(glm::mat4 scale_matrix)
	{
		scale_matrix_ = scale_matrix;
	}

	void Terrain::LoadHeightmapFromFile(std::string file_name)
	{
		// Load the image
		terrain_heightmap_.loadFromFile(file_name);
		sf::Vector2u size = terrain_heightmap_.getSize();
		rows_ = size.x;
		cols_ = size.y;

		// Load our shaders
		std::vector<tdogl::Shader> shaders;
		shaders.push_back(vertex_shader_.GetAsset());
		shaders.push_back(fragment_shader_.GetAsset());
		terrain_shader_ = new tdogl::ShaderProgram(shaders);
		terrain_shader_->use();

		// Determine how many times to map a texture accross a dimension
		const float percent = 0.05f;
		float tex_u_scale = float(cols_)*percent;
		float tex_v_scale = float(rows_)*percent;

		// Transform the image into a set of height coordinates, between -1 and 1
		for (unsigned int z = 0; z < rows_; ++z)
		{
			for (unsigned int x = 0; x < cols_; ++x)
			{
				// Calculate Grayscale for height and scale grid coordinates
				float gray = GrayScale(terrain_heightmap_.getPixel(x, z));
				float x_scaled = ScaleToOpenGLCoordinates(float(x), float(cols_));
				float y_scaled = gray / 255;
				float z_scaled = ScaleToOpenGLCoordinates(float(z), float(rows_));

				// Store in our vertex for now
				vertex_data_.push_back(glm::vec3(x_scaled,y_scaled, z_scaled)) ;
				texture_data_.push_back(glm::vec2(tex_u_scale*x_scaled, tex_v_scale*z_scaled));

				// Calculate and store element data for our triangle strip
				AddElementCoords(x, z);
			}

			StitchTriangleStrip(z);
		}

		// Finally, terrain needs lighting, so, we'll add lighting.
		// Phong reflection Model
		// Ambient, Diffuse and Specular
		// Calculate Normals
		CalculateNormals();


		//Create a Vertex Array Object
		// once bound, all calls to glVertexAttribPointer
		// are stored in the vertex array object, so you can switch.
		glGenVertexArrays(1, &vao_);
		glBindVertexArray(vao_);


		// Combine our data
		for (unsigned int z = 0; z < rows_; ++z)
		{
			for (unsigned int x = 0; x < cols_; ++x)
			{
				int index = Transform2Dto1D(z, x);
				vertex_texture_normal_data_.AddData(&vertex_data_[index], sizeof(glm::vec3));
				vertex_texture_normal_data_.AddData(&texture_data_[index], sizeof(glm::vec2));
				vertex_texture_normal_data_.AddData(&normal_data_[index], sizeof(glm::vec3));
			}
		}

		// Load our terrain to the graphics card
		vertex_texture_normal_data_.BindBuffer(GL_ARRAY_BUFFER);
		vertex_texture_normal_data_.UploadDataToGPU(GL_STATIC_DRAW);

		// Load the element data onto the graphics card
		element_data_.BindBuffer(GL_ELEMENT_ARRAY_BUFFER);
		element_data_.UploadDataToGPU(GL_STATIC_DRAW);

		// Specify the layout of the vertex data (note vec3 for vertex + vec2 for texture + vec3 for normal)
		glEnableVertexAttribArray(terrain_shader_->attrib("vert"));
		glVertexAttribPointer(terrain_shader_->attrib("vert"), 3, GL_FLOAT, GL_FALSE, 2 * sizeof(glm::vec3) + sizeof(glm::vec2), NULL);

		// Specify the texture data
		glEnableVertexAttribArray(terrain_shader_->attrib("vert_tex_coord"));
		glVertexAttribPointer(terrain_shader_->attrib("vert_tex_coord"), 2, GL_FLOAT, GL_FALSE, 2 * sizeof(glm::vec3) + sizeof(glm::vec2), (const GLvoid*)(sizeof(glm::vec3)));

		// Specify the normal data
		glEnableVertexAttribArray(terrain_shader_->attrib("vert_normal"));
		glVertexAttribPointer(terrain_shader_->attrib("vert_normal"), 2, GL_FLOAT, GL_FALSE, 2 * sizeof(glm::vec3) + sizeof(glm::vec2), (const GLvoid*)(sizeof(glm::vec3)+sizeof(glm::vec2)));
		

		//TODO: add texture to uniform tex
		sandgrass_texture_.GetAsset().SetWrapMode(GL_REPEAT);
		sand_texture_.GetAsset().SetWrapMode(GL_REPEAT);
		rock_texture_.GetAsset().SetWrapMode(GL_REPEAT);
		water_texture_.GetAsset().SetWrapMode(GL_REPEAT);

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, sandgrass_texture_.GetAsset().Object());
		terrain_shader_->setUniform("tex_sandgrass", 0); //set to 0 because the texture is bound to GL_TEXTURE0

		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, sand_texture_.GetAsset().Object());
		terrain_shader_->setUniform("tex_sand", 1); //set to 0 because the texture is bound to GL_TEXTURE0

		glActiveTexture(GL_TEXTURE2);
		glBindTexture(GL_TEXTURE_2D, rock_texture_.GetAsset().Object());
		terrain_shader_->setUniform("tex_rock", 2); //set to 0 because the texture is bound to GL_TEXTURE0

		glActiveTexture(GL_TEXTURE3);
		glBindTexture(GL_TEXTURE_2D, water_texture_.GetAsset().Object());
		terrain_shader_->setUniform("tex_water", 3); //set to 0 because the texture is bound to GL_TEXTURE0

		glBindVertexArray(0);
	}

	void Terrain::Draw(tdogl::Camera &camera, cn::Lighting& lighting)
	{
		glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
		glEnable(GL_DEPTH_TEST);

		glEnable(GL_CULL_FACE);

		//glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		glBindVertexArray(vao_);
		terrain_shader_->use();

		terrain_shader_->setUniform("camera", camera.matrix());
		terrain_shader_->setUniform("scale", scale_matrix_);

		terrain_shader_->setUniform("light.position", lighting.GetLightSource().position);
		terrain_shader_->setUniform("light.intensities", lighting.GetLightSource().intensities);

		int size = (rows_ - 1)*(cols_ - 1) * 4;
		glDrawElements(GL_TRIANGLE_STRIP, size, GL_UNSIGNED_INT, 0);
		

		terrain_shader_->stopUsing();
	}

	float Terrain::ScaleToOpenGLCoordinates(float i, float max)
	{
		float half_max = max / 4.0f;
		i -= half_max;
		i /= half_max;
		return i / 2;
	}

	float Terrain::GrayScale(sf::Color pixel)
	{
		return float(0.299 * pixel.r + 0.587 * pixel.g + 0.114 * pixel.b);
	}

	void Terrain::AddElementCoords(unsigned int x, unsigned int z)
	{
		// Store the values for our element buffer which defines a Triangle Strip
		// B D F   ^
		// |\|\|  x|__>
		// A C E    z
		// but we're doing A, C, E, etc... so we'll just make our
		// element buffer each iteration with that knowledge
		if (x + 1 < cols_ && z + 1 < rows_)
		{
			GLuint a = Transform2Dto1D(z,x); // current vertex
			GLuint b = Transform2Dto1D(z+1, x); // vertex a row ahead
			GLuint c = Transform2Dto1D(z, x+1); // next vertex
			GLuint d = Transform2Dto1D(z + 1, x + 1); // vertex a row and a column ahead
	
			element_data_.AddData(&a, sizeof(GLuint));
			element_data_.AddData(&b, sizeof(GLuint));
			element_data_.AddData(&c, sizeof(GLuint));
			element_data_.AddData(&d, sizeof(GLuint));
		}
	}

	void Terrain::StitchTriangleStrip(unsigned int z)
	{
		// Now at the end of every row, we need to add a degenerate triangle (triangle with no area)
		// in order to stitch the strips together
		if (z < rows_ - 1)
		{
			GLuint last_d = Transform2Dto1D(z + 1, cols_ - 1);
			GLuint first_b = Transform2Dto1D(z + 1, 0);
			element_data_.AddData(&last_d, sizeof(GLuint));
			element_data_.AddData(&first_b, sizeof(GLuint));
		}
	}

	void Terrain::CalculateNormals()
	{
		//  M\---N\---O\---P
		//	| \  | \  | \  |
		//	|  \ |  \ |  \ |
		//	I\--\J\---K\--\L
		//	| \  | \  | \  |
		//	|  \ |  \ |  \ |
		//	B\--\D\---F\--\H
		//	| \  | \  | \  |
		//	|  \ |  \ |  \ |
		//	A---\C---\E---\G

		// Looking at J, how would we get its normal?
		// Noting that the normal of the triangle MNJ can be different than that of NKJ
		// I see a few ways of doing this, but what I'm finding online is as follows: 
		// normalized sum of the surrounding triangles normal's
		// Another perhaps easier way, would be just to get the normal of the surrounding
		// plane, eg IKEA for D?  he he he
		// This site outlines where I got the idea: https://www.opengl.org/discussion_boards/showthread.php/163809-heightmap-geometry-normals
		// This site has essentially what I've done http://www.mbsoftworks.sk/index.php?page=tutorials&series=1&tutorial=24
		// However, their diagram is a bit incorrect

		// Pass 1, calculate normal for every triangle (similar to element data)
		std::vector<glm::vec3> normals;
		for (unsigned int z = 0; z < rows_-1; ++z)
		{
			for (unsigned int x = 0; x < cols_-1; ++x)
			{
				//calculate first triangle (for example ABC, CDE, EFG, BID, etc..)
				glm::vec3 triangle_a[3] = {
					vertex_data_[Transform2Dto1D(z, x + 1)],
					vertex_data_[Transform2Dto1D(z, x)],
					vertex_data_[Transform2Dto1D(z + 1, x)]
				};

				//calculate second triangle (for example BDC, DFE, FHG, IJD, etc..)
				glm::vec3 triangle_b[3] = {
					vertex_data_[Transform2Dto1D(z + 1, x)],
					vertex_data_[Transform2Dto1D(z + 1, x + 1)],
					vertex_data_[Transform2Dto1D(z, x + 1)]
				};
						
				// Calculate the normal for each of our 2 triangles, using the cross product
				glm::vec3 norm_a = glm::cross(triangle_a[0] - triangle_a[1], triangle_a[1] - triangle_a[2]);
				glm::vec3 norm_b = glm::cross(triangle_b[0] - triangle_b[1], triangle_b[1] - triangle_b[2]);

				// normalize
				normals.push_back(glm::normalize(norm_a));
				normals.push_back(glm::normalize(norm_b));

			}
		}


		// Pass 2, calculate normalized average of surrounding normals
		// Going back to the grid, each interior vertex has 6 surrounding triangles
		for (unsigned int z = 0; z < rows_; ++z)
		{
			for (unsigned int x = 0; x < cols_; ++x)
			{
				glm::vec3 final_norm = glm::vec3(0.0f, 0.0f, 0.0f);

				// refering to the diagram, 
				unsigned int top_left = Transform2Dto1D(z, x-1, rows_-1) * 2; //top left of D, would be stored in B
				unsigned int top_right = Transform2Dto1D(z, x, rows_ - 1) * 2; //bottom right of D, would be stored in D itself
				unsigned int bottom_left = Transform2Dto1D(z - 1, x - 1, rows_ - 1) * 2; //bottom right of D, would be stored in A
				unsigned int bottom_right = Transform2Dto1D(z - 1, x, rows_ - 1) * 2; //bottom right of D, would be stored in C
				
				//Note: bottom values will be jibberish when z - 1 <0 or x-1 < 0, since we're using unsigned ints
				/*if (top_left > normals.size() ||
					top_right > normals.size()) 
				{
					std::cout << "error" << std::endl;
				}*/

				// top left triangles
				if (x > 0 && z < rows_-1)
				{
					final_norm += normals[top_left];
					final_norm += normals[top_left + 1];
				}		
				// top right triangle
				if (x < cols_-1 && z < rows_ - 1)
				{
					final_norm += normals[top_right];
				}
				// bottom left triangle
				if (x > 0 && z > 0)
				{
					final_norm += normals[bottom_left + 1];
				}
				// bottom right triangles
				if (x < rows_-1 && z > 0)
				{
					final_norm += normals[bottom_right];
					final_norm += normals[bottom_right + 1];
				}

				normal_data_.push_back(glm::normalize(final_norm));

			}
		}
	}

	unsigned int Terrain::Transform2Dto1D(unsigned int z, unsigned int x)
	{
		return  z * rows_ + x;
	}
	unsigned int Terrain::Transform2Dto1D(unsigned int z, unsigned int x, unsigned int rows)
	{
		return  z * rows + x;
	}
}