#include "Header/Engine/Interfaces/IEngine.hpp"
#include "Header/Engine/Interfaces/IScreen.hpp"
#include "Header/Engine/Screens/SplashScreen.hpp"
#include "WorldScreen.hpp"


namespace cn
{

	WorldScreen::WorldScreen(IEngine& engine, typeAssetID world_id) :
		IScreen(world_id, engine),
		world_id_(world_id),
		basic_heightmap_(),
		walking_asset_("C:/Users/Cory/Google Drive/Code/Resources/Sound/walking_foliage.wav")
	{
		// setup gCamera
		mEngine.camera_.lookAt(glm::vec3(1, 0, 1));
		mEngine.camera_.setPosition(glm::vec3(0, 50, 1)); // start a little bit back
		mEngine.camera_.setNearAndFarPlanes(0.05f, 500.0f);

		walking_sound_.setBuffer(walking_asset_.GetAsset());
		
	}


	WorldScreen::~WorldScreen()
	{
	}

	
	sf::Vector2i mouse_prev;
	void WorldScreen::HandleEvents(sf::Event theEvent)
	{
		
		// Exit program if Escape key is pressed
		if ((theEvent.type == sf::Event::KeyReleased) &&
			(theEvent.key.code == sf::Keyboard::Escape)
			)
		{
			// Signal the application to exit
			mEngine.Quit(cn::StatusEngineOK);
		}
		else if (theEvent.type == sf::Event::MouseButtonPressed)
		{
			mouse_prev = sf::Mouse::getPosition();
		}
		else if (theEvent.type == sf::Event::KeyPressed)
		{

		}
		else if (theEvent.type == sf::Event::KeyReleased)
		{	
			walking_sound_.pause();
		}
	}

	void WorldScreen::DoInit(void)
	{
		// First call our base class CN_API implementation
		IScreen::DoInit();
		
		LightSource light;
		light.position = mEngine.camera_.position(); // start with the camera
		light.intensities = glm::vec3(1, 1, 1); //white
		lighting_.AddLightSource(light);
				
		basic_heightmap_.LoadHeightmapFromFile("C:/Users/Cory/Google Drive/Code/Resources/Images/terrain.bmp");


	}

	void WorldScreen::ReInit(void)
	{
		// Do nothing yet
	}

	void WorldScreen::UpdateFixedTimeStep(float t, float dt)
	{
		//Using states or something of that nature is probably better than keys_down_

		//move position of camera based on WASD keys
		
		const float moveSpeed = 50.0f; //units per dt //should be closer to scale size
		const float mouseSensitivity = 0.1f;

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::S)){			
			PlaySound();
			mEngine.camera_.offsetPosition(dt * moveSpeed * -mEngine.camera_.forward());
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::W)){
			PlaySound();
			mEngine.camera_.offsetPosition(dt * moveSpeed * mEngine.camera_.forward());
		}
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::A)){
			PlaySound();
			mEngine.camera_.offsetPosition(dt * moveSpeed * -mEngine.camera_.right());
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::D)){
			PlaySound();
			mEngine.camera_.offsetPosition(dt * moveSpeed * mEngine.camera_.right());
		}
		else {
			
		
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::E)){	
			LightSource light;
			light.position = mEngine.camera_.position(); // start with the camera
			light.intensities = glm::vec3(1, 1, 1); //white
			lighting_.AddLightSource(light);
		}
		
		if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
		{

			sf::Vector2i mouse_cur = sf::Mouse::getPosition();
			sf::Vector2i mouse_diff = mouse_cur - mouse_prev;
			mouse_prev = mouse_cur;
			mEngine.camera_.offsetOrientation(mouseSensitivity * mouse_diff.y, mouseSensitivity * mouse_diff.x);
		}

	
	}

	void WorldScreen::UpdateVariableTimeStep(float t, float frame_time)
	{
		// nothing to do
	}

	void WorldScreen::RemoveTemporalAliasing(double alpha)
	{
		//not necesary for a still image
	}

	void WorldScreen::Draw(void)
	{


		//std::cout << "test" << endl;
		basic_heightmap_.Draw(mEngine.camera_, lighting_);
	}

	void WorldScreen::HandleCleanup(void)
	{

	}


	void WorldScreen::PlaySound()
	{
		if (walking_sound_.getStatus() != sf::Sound::Playing)
			walking_sound_.play();
	}
} // namespace cn


