#ifndef   CN_WORLD_SCREEN_HPP_INCLUDED
#define   CN_WORLD_SCREEN_HPP_INCLUDED

#include "Terrain.hpp"
#include "Header/Engine/Interfaces/IScreen.hpp"
#include "Header/Engine/Classes/Camera.hpp"

namespace cn {

	class WorldScreen :
		public cn::IScreen
	{
	public:
		WorldScreen(IEngine& engine, typeAssetID world_id);

		virtual ~WorldScreen();


		/**
		* DoInit is responsible for initializing this State
		*/
		virtual void DoInit(void);

		/**
		* ReInit is responsible for Reseting this state when the
		* StateManager::ResetActiveState() method is called.  This way a Game
		* State can be restarted without unloading and reloading the game assets
		*/
		virtual void ReInit(void);

		/**
		* HandleEvents is responsible for handling input events for this
		* State when it is the active State.
		* @param[in] theEvent to process from the Engine class CN_API Loop method
		*/
		virtual void HandleEvents(sf::Event theEvent);

		/**
		* UpdateFixed is responsible for handling all State fixed update needs for
		* this State when it is the active State.
		*/
		virtual void UpdateFixedTimeStep(float t, float dt);

		/**
		* UpdateVariable is responsible for handling all State variable update
		* needs for this State when it is the active State.
		* @param[in] theElapsedTime since the last Draw was called
		*/
		virtual void UpdateVariableTimeStep(float t, float frame_time);

		/**
		* RemoveTemporalAliasing separate from UpdateVariableTimeStep
		* to specifically prompt the implementer to do so
		* Screen state = currentScreen*alpha + previousScreen * (1.0 - alpha);
		* @param[in] theElapsedTime since the last Draw was called
		*/
		virtual void RemoveTemporalAliasing(double alpha);

		/**
		* Draw is responsible for handling all Drawing needs for this State
		* when it is the Active State.
		*/
		virtual void Draw(void);

	protected:
		/**
		* HandleCleanup is responsible for performing any cleanup required
		* before this State is removed.
		*/
		virtual void HandleCleanup(void);

	private:

		void PlaySound();

		// Variables
		/////////////////////////////////////////////////////////////////////////
		/// The Asset ID to assign to the splash image
		typeAssetID		world_id_;

		Terrain basic_heightmap_;
		SoundAsset walking_asset_;
		sf::Sound walking_sound_;

		Lighting lighting_;

		
	};
} //namespace cn

#endif //CN_WORLD_SCREEN_HPP_INCLUDED